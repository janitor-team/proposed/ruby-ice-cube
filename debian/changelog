ruby-ice-cube (0.16.4-3) unstable; urgency=medium

  * Team upload
  * Add upstream to support Psych 4 shipped with ruby3.1 (Closes: #1019632)
  * Fix some round trip test failures in autopkgtest
  * Bump Standards-Version to 4.6.1 (no changes needed)
  * Skipping -2 to recover from broken upload

 -- Cédric Boutillier <boutil@debian.org>  Thu, 17 Nov 2022 00:05:35 +0100

ruby-ice-cube (0.16.4-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Update standards version to 4.5.1, no changes needed.

  [ Cédric Boutillier ]
  * New upstream version 0.16.4
  * update ustream metadata
  * Bump Standards-Version to 4.6.0 (no changes needed)
  * Use github page as homepage
  * Drop skip-failing-specs-yaml.patch, fixed upstream

 -- Cédric Boutillier <boutil@debian.org>  Wed, 17 Nov 2021 09:39:56 +0100

ruby-ice-cube (0.16.3-1) unstable; urgency=medium

  * Team upload

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Remove duplicate line from changelog.
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Cédric Boutillier ]
  * Use https:// in Vcs-* fields
  * New upstream version 0.16.3
    + removes Fixnum deprecation warning (Closes: #958557)
  * update packaging
  * Merge patches into clean-spec-helper patch
  * update ruby-tests.rake
  * Use Github as the source to fetch the specs
  * Remove Simplecov patch
  * Add skip-failing-specs-yaml.patch to skip failing tests due to
    summer/winter time mess

 -- Cédric Boutillier <boutil@debian.org>  Wed, 29 Jul 2020 00:34:39 +0200

ruby-ice-cube (0.12.1-1) unstable; urgency=medium

  * Upstream update to version 0.12.1
  * Fix incorrect patching in source git (remove already applied patch)
  * Fixed the relative path of lib/ice_cube.rb file in spec_helper

 -- Balasankar C <balasankarc@autistici.org>  Tue, 20 Jan 2015 21:07:22 +0530

ruby-ice-cube (0.11.1-1) unstable; urgency=medium

  * Initial release (Closes: #770475)

 -- Balasankar C <balasankarc@autistici.org>  Thu, 20 Nov 2014 19:41:45 +0530
